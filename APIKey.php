<?php
declare(strict_types=1);

namespace DBIP;

class APIKey
{
    static public string $defaultApiKey = DEFAULT_API_KEY;

    static public function set(string $apiKey): void
    {
        self::$defaultApiKey = $apiKey;
    }

    static public function info(): \stdClass
    {
        return Client::getInstance()->getKeyInfo();
    }
}