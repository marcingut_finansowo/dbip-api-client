<?php
declare(strict_types=1);

namespace DBIP;

class Address
{
    static public function lookup(string $addr = "self"): \stdClass
    {
        return Client::getInstance()->getAddressInfo($addr);
    }
}