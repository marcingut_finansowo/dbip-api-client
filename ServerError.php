<?php
declare(strict_types=1);

namespace DBIP;

class ServerError extends \Exception
{
    public function __construct(string $message, private string $errorCode)
    {
        parent::__construct($message);
    }

    public function getErrorCode(): string
    {
        return $this->errorCode;
    }
}