<?php
declare(strict_types=1);

namespace DBIP;

class ASN
{
    static public function lookup(mixed $asNumber): \stdClass
    {
        return Client::getInstance()->getASInfo($asNumber);
    }
}